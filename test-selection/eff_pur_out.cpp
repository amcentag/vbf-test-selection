#include "../histo.h"
#include <string>
#include <iomanip>
#include <fstream>

using namespace std;

// return weight in selection region for specified production mode, eta cutoff, m cutoff
double get_mcwt_sel(string procname, double deta_cutoff, double m_cutoff) {
	unique_ptr<TFile> f(TFile::Open(("~/vbf/ntuples/"+procname+".root").c_str()));
	auto tree = f->Get<TTree>("Nominal");
	double mcwt_sel = 0;

	double mcwt=0; tree->SetBranchAddress("weight",&mcwt);
	double ptH=0; tree->SetBranchAddress("HCand_pT",&ptH);
	double etaH=0; tree->SetBranchAddress("HCand_Eta",&etaH);
	double phiH=0; tree->SetBranchAddress("HCand_Phi",&phiH);
	double mH=0; tree->SetBranchAddress("HCand_M",&mH);
	vector<double> *ptsmj=0; tree->SetBranchAddress("SmallRJets_pT",&ptsmj);
	vector<double> *etasmj=0; tree->SetBranchAddress("SmallRJets_eta",&etasmj);
	vector<double> *phismj=0; tree->SetBranchAddress("SmallRJets_phi",&phismj);
	vector<double> *msmj=0; tree->SetBranchAddress("SmallRJets_M",&msmj);
	double ptj1=0; tree->SetBranchAddress("Jet1_pT",&ptj1);
	double etaj1=0; tree->SetBranchAddress("Jet1_eta",&etaj1);
	double phij1=0; tree->SetBranchAddress("Jet1_phi",&phij1);
	double mj1=0; tree->SetBranchAddress("Jet1_M",&mj1);
	double ptj2=0; tree->SetBranchAddress("Jet2_pT",&ptj2);
	double etaj2=0; tree->SetBranchAddress("Jet2_eta",&etaj2);
	double phij2=0; tree->SetBranchAddress("Jet2_phi",&phij2);
	double mj2=0; tree->SetBranchAddress("Jet2_M",&mj2);
	int numlrj=0; tree->SetBranchAddress("nLargeRJets",&numlrj);
	float hxbb=0; tree->SetBranchAddress("HCand_XbbScore",&hxbb);

	for (int e=0; e<tree->GetEntries(); e++) {
		tree->GetEntry(e);
		// event preselection
		if ( hxbb>2.44 and ptj1>450 and ptj2>250 and numlrj>1 ) {
			TLorentzVector pH,pj1,pj2,pj_H,pj_nH;
			pH.SetPtEtaPhiM(ptH,etaH,phiH,mH);
			pj1.SetPtEtaPhiM(ptj1,etaj1,phij1,mj1);
			pj2.SetPtEtaPhiM(ptj2,etaj2,phij2,mj2);
			// assign H and non-H LRJs
			if ( pj1.DeltaR(pH) < 1 ) { pj_H = pj1; pj_nH = pj2; }
			else if ( pj2.DeltaR(pH) < 1 ) { pj_H = pj2; pj_nH = pj1; }
			double *ptsmjd = ptsmj->data();
			double *etasmjd = etasmj->data();
			double *phismjd = phismj->data();
			double *msmjd = msmj->data();
			bool qjet_found = 0;
			double deta_qjets = 0;
			double mjj_qjets = 0;
			// loop through SRJs to find which one might be the secondary VBF quark
			for (int j=0; j<ptsmj->size(); j++) {
				TLorentzVector pjtest;
				pjtest.SetPtEtaPhiM(ptsmjd[j],etasmjd[j],phismjd[j],msmjd[j]);
				if ( pjtest.DeltaR(pH)>1 and abs(etasmjd[j]-pj_nH.Eta())>1.4
					 and ptsmjd[j]>25 ) {
					qjet_found = 1;
					mjj_qjets = (pj_nH + pjtest).M();
					deta_qjets = abs(etasmjd[j]-pj_nH.Eta());
					break;
				}
			}
			if ( qjet_found and mjj_qjets > m_cutoff and deta_qjets > deta_cutoff) { 
				mcwt_sel += mcwt; 
			}
		}
	}

	return mcwt_sel;
}

// return weight in CMS selection region for VBF 
double get_mcwt_sel_cms(string procname) {
	unique_ptr<TFile> f(TFile::Open(("~/vbf/ntuples/"+procname+".root").c_str()));
	auto tree = f->Get<TTree>("Nominal");
	double mcwt_sel = 0;

	double mcwt=0; tree->SetBranchAddress("weight",&mcwt);
	double ptH=0; tree->SetBranchAddress("HCand_pT",&ptH);
	double etaH=0; tree->SetBranchAddress("HCand_Eta",&etaH);
	double phiH=0; tree->SetBranchAddress("HCand_Phi",&phiH);
	double mH=0; tree->SetBranchAddress("HCand_M",&mH);
	vector<double> *ptsmj=0; tree->SetBranchAddress("SmallRJets_pT",&ptsmj);
	vector<double> *etasmj=0; tree->SetBranchAddress("SmallRJets_eta",&etasmj);
	vector<double> *phismj=0; tree->SetBranchAddress("SmallRJets_phi",&phismj);
	vector<double> *msmj=0; tree->SetBranchAddress("SmallRJets_M",&msmj);
	double ptj1=0; tree->SetBranchAddress("Jet1_pT",&ptj1);
	double etaj1=0; tree->SetBranchAddress("Jet1_eta",&etaj1);
	double phij1=0; tree->SetBranchAddress("Jet1_phi",&phij1);
	double mj1=0; tree->SetBranchAddress("Jet1_M",&mj1);
	double ptj2=0; tree->SetBranchAddress("Jet2_pT",&ptj2);
	double etaj2=0; tree->SetBranchAddress("Jet2_eta",&etaj2);
	double phij2=0; tree->SetBranchAddress("Jet2_phi",&phij2);
	double mj2=0; tree->SetBranchAddress("Jet2_M",&mj2);
	float hxbb=0; tree->SetBranchAddress("HCand_XbbScore",&hxbb);

	for (int e=0; e<tree->GetEntries(); e++) {
		tree->GetEntry(e);
		// event preselection
		if ( hxbb>2.44 and ptj1>450 ) {
			TLorentzVector pH,pj1,pj2,pj_H,pjq1,pjq2;
			pH.SetPtEtaPhiM(ptH,etaH,phiH,mH);
			pj1.SetPtEtaPhiM(ptj1,etaj1,phij1,mj1);
			pj2.SetPtEtaPhiM(ptj2,etaj2,phij2,mj2);
			// assign H and non-H LRJs
			if ( pj1.DeltaR(pH) < 1 ) { pj_H = pj1; }
			else if ( pj2.DeltaR(pH) < 1 ) { pj_H = pj2; }
			double *ptsmjd = ptsmj->data();
			double *etasmjd = etasmj->data();
			double *phismjd = phismj->data();
			double *msmjd = msmj->data();
			double nsrj_oH = 0; // number of SRJs outside H
			// loop through SRJs to get leading 2 (in pT)
			for (int j=0; j<ptsmj->size(); j++) {
				TLorentzVector pjtest;
				pjtest.SetPtEtaPhiM(ptsmjd[j],etasmjd[j],phismjd[j],msmjd[j]);
				if ( pjtest.DeltaR(pH)>0.8 ) { nsrj_oH++; }
				if ( nsrj_oH == 1 ) { pjq1 = pjtest; continue; }
				if ( nsrj_oH == 2 ) { pjq2 = pjtest; break; }
			} 
			if ( nsrj_oH == 2 ) {
				double deta_qjets = abs(pjq1.Eta()-pjq2.Eta());
				double mjj_qjets = (pjq1 + pjq2).M();
				if ( deta_qjets > 3.5 and mjj_qjets > 1000 ) {
					mcwt_sel += mcwt;
				}
			}
		}
	}

	return mcwt_sel;
}

// return total mcwt from all events in ntuple passing preselction
double get_mcwt_total(string procname) {
	unique_ptr<TFile> f(TFile::Open(("~/vbf/ntuples/"+procname+".root").c_str()));
	auto tree = f->Get<TTree>("Nominal");
	double mcwt_total = 0;

	double mcwt=0; tree->SetBranchAddress("weight",&mcwt);
	double ptj1=0; tree->SetBranchAddress("Jet1_pT",&ptj1);
	double ptj2=0; tree->SetBranchAddress("Jet2_pT",&ptj2);
	float hxbb=0; tree->SetBranchAddress("HCand_XbbScore",&hxbb);

	for (int e=0; e<tree->GetEntries(); e++) {
		tree->GetEntry(e);
		if ( hxbb>2.44 and ptj1>450 and ptj2>250 ) { mcwt_total += mcwt; }
	}

	return mcwt_total;
}

/*
// loop through different values of deta_cutoff and get purity and efficiency for each
void eff_pur_out() {
	double deta_cutoff = 4.0; // minimum eta distance outside non-H LRJ where we search for primary q SRJ
	double m_cutoff = 800; // minimum mjj of selected primary quark jets
	double mcwt_total_VBF = get_mcwt_total("VBF");
	cout << fixed << setprecision(4);
	cout << "Total weight in VBF ntuple: " << mcwt_total_VBF << endl;

	ofstream outf;
	outf.open("data_eff_pur_40.csv");
	outf << "//deta_cutoff,eff,pur,mcwt_total_VBF,mcwt_sel_VBF,mcwt_sel_VH,mcwt_sel_ggH,mcwt_sel_ttH\n";
	outf.close();

	//for (deta_cutoff=2.5; deta_cutoff<4.5; deta_cutoff+=0.1) {
	for (m_cutoff=800; m_cutoff<1600; m_cutoff+=100) {
		cout << "Calculating efficiency and purity for m_cutoff=" << m_cutoff << ", deta_cutoff=" << deta_cutoff << endl;
		double mcwt_sel_VBF = get_mcwt_sel("VBF",deta_cutoff,m_cutoff);
		cout << "VBF mcwt: " << mcwt_sel_VBF << endl;
		double mcwt_sel_VH = get_mcwt_sel("VH",deta_cutoff,m_cutoff);
		cout << "VH mcwt: " << mcwt_sel_VH << endl;
		double mcwt_sel_ggH = get_mcwt_sel("ggH",deta_cutoff,m_cutoff);
		cout << "ggH mcwt: " << mcwt_sel_ggH << endl;
		double mcwt_sel_ttH = get_mcwt_sel("ttH",deta_cutoff,m_cutoff);
		cout << "ttH mcwt: " << mcwt_sel_ttH << endl;

		double pur = mcwt_sel_VBF/(mcwt_sel_VBF + mcwt_sel_VH + mcwt_sel_ggH + mcwt_sel_ttH);
		double eff = mcwt_sel_VBF/mcwt_total_VBF;
		cout << "Efficiency = " << eff << ", purity = " << pur << endl;

		outf.open("data_eff_pur_40.csv", ios::app);
		outf << m_cutoff << "," << eff << "," << pur << "," << mcwt_total_VBF << "," << 
			 mcwt_sel_VBF << "," << mcwt_sel_VH << "," << mcwt_sel_ggH << "," << mcwt_sel_ttH << endl;
		outf.close();
	}
}
*/

// code to get purity and efficiency for CMS selection region
void eff_pur_out() {
	cout << fixed << setprecision(4);
	cout << "Calculating efficiency and purity for CMS selection region" << endl;

	double mcwt_total_VBF = get_mcwt_total("VBF");
	cout << "Total weight in VBF ntuple: " << mcwt_total_VBF << endl;

	double mcwt_sel_VBF = get_mcwt_sel_cms("VBF");
	cout << "VBF mcwt: " << mcwt_sel_VBF << endl;
	double mcwt_sel_VH = get_mcwt_sel_cms("VH");
	cout << "VH mcwt: " << mcwt_sel_VH << endl;
	double mcwt_sel_ggH = get_mcwt_sel_cms("ggH");
	cout << "ggH mcwt: " << mcwt_sel_ggH << endl;
	double mcwt_sel_ttH = get_mcwt_sel_cms("ttH");
	cout << "ttH mcwt: " << mcwt_sel_ttH << endl;

	double pur = mcwt_sel_VBF/(mcwt_sel_VBF + mcwt_sel_VH + mcwt_sel_ggH + mcwt_sel_ttH);
	double eff = mcwt_sel_VBF/mcwt_total_VBF;
	cout << "Efficiency = " << eff << ", purity = " << pur << endl;
}
