// run analysis for VBF ntuple containing truth-level info

#include "../histo.h"
#include <string>

using namespace std;

const double pi = 3.14159265359;

void truthhistos() {
	unique_ptr<TFile> f(TFile::Open("~/vbf/ntuples/VBFtruth.root"));
	auto tree = f->Get<TTree>("Nominal");

	double mcwt=0; tree->SetBranchAddress("weight",&mcwt);
	double ptH=0; tree->SetBranchAddress("HCand_pT",&ptH);
	double etaH=0; tree->SetBranchAddress("HCand_Eta",&etaH);
	double phiH=0; tree->SetBranchAddress("HCand_Phi",&phiH);
	double mH=0; tree->SetBranchAddress("HCand_M",&mH);
	double ptj1=0;tree->SetBranchAddress("Jet1_pT",&ptj1);
	double etaj1=0; tree->SetBranchAddress("Jet1_eta",&etaj1);
	double phij1=0; tree->SetBranchAddress("Jet1_phi",&phij1);
	double mj1=0; tree->SetBranchAddress("Jet1_M",&mj1);
	double ptj2=0; tree->SetBranchAddress("Jet2_pT",&ptj2);
	double etaj2=0; tree->SetBranchAddress("Jet2_eta",&etaj2);
	double phij2=0; tree->SetBranchAddress("Jet2_phi",&phij2);
	double mj2=0; tree->SetBranchAddress("Jet2_M",&mj2);
	vector<double> *ptsrj=0; tree->SetBranchAddress("SmallRJets_pT",&ptsrj);
	vector<double> *etasrj=0; tree->SetBranchAddress("SmallRJets_eta",&etasrj);
	vector<double> *phisrj=0; tree->SetBranchAddress("SmallRJets_phi",&phisrj);
	vector<double> *msrj=0; tree->SetBranchAddress("SmallRJets_M",&msrj);
	vector<int> *idTr=0; tree->SetBranchAddress("truthpart_pdgId",&idTr);
	vector<int> *statTr=0; tree->SetBranchAddress("truthpart_status",&statTr);
	vector<double> *ptTr=0; tree->SetBranchAddress("truthpart_pT",&ptTr);
	vector<double> *etaTr=0; tree->SetBranchAddress("truthpart_eta",&etaTr);
	vector<double> *phiTr=0; tree->SetBranchAddress("truthpart_phi",&phiTr);
	vector<double> *mTr=0; tree->SetBranchAddress("truthpart_m",&mTr);
	int numlrj=0; tree->SetBranchAddress("nLargeRJets",&numlrj);
	
	//vector<string> histonames = {"delEta_smj12","2lrj_delR_j2_smj12"};
	//vector<double> histobins = {20,0,4,24,0,2*pi};
	//histo hsel(histonames, "VBFtruth_sel", histobins);
	
	// setup histos
	// histo info contained in histotitles vector
	TH1D *hdEta = new TH1D("hdEta","delEta between truth primary quarks q1,q2",40,0,8);
	vector<string> histonames = {"hdRnearq","hdRfarq","hdRqq","hwsrjq","hqsrjEta","hqsrjpT",
								 "hsrjEta","hsrjpT","hmqq","hmjj","hdPhiqq"};
	vector<string> histotitles = {"delR between non-Higgs LRJ and nearest q",
		"delR between non-Higgs LRJ and farthest q","delR between primary vertex quarks",
		"Which SRJ matches the non-LRJ q","non-LRJ q eta","non-LRJ q pT",
		"matched-to-q SRJ eta","matched-to-q SRJ pT","diquark system inv mass",
		"matched jet system inv mass","dPhi between truth primary quarks"};
	vector<double> histobins = {24,0,2*pi, 24,0,2*pi, 24,0,2*pi, 7,-2.5,4.5, 20,0,4, 25,0,500,
								20,0,4, 25,0,500, 20,0,2000, 20,0,2000, 24,0,pi};
	histo hlrj(histonames, histotitles, "VBFtruth_sel", histobins);

	cout << "Num entries: " << tree->GetEntries() << endl;
	for (int i=0; i<tree->GetEntries(); i++) {
		tree->GetEntry(i);
		double dEta_smj = -1;
		int q1 = -1; int q2 = -1; // which entry in truth data corresponds to primary quarks
		int *idTrd = idTr->data(); // truth data vectors
		int *statTrd = statTr->data();
		double *phiTrd = phiTr->data();
		double *etaTrd = etaTr->data();
		double *ptTrd = ptTr->data();
		double *mTrd = mTr->data();
		double *ptsrjd = ptsrj->data();
		double *etasrjd = etasrj->data();
		double *phisrjd = phisrj->data();
		double *msrjd = msrj->data();

		for (int p=0; p<idTr->size(); p++) { // find the primary quarks in truth data
			if (statTrd[p]==23 and idTrd[p]!=25 and q1==-1) { q1 = p; }
			else if (statTrd[p]==23 and idTrd[p]!=25 and q1!=-1 and q2==-1) { q2 = p; }
			else if (q1!=-1 and q2!=-1) { break; }
			else { continue; }
		}

		if (q2!=-1 and ptj1>450 and ptj2>250) {
			dEta_smj = abs(etaTrd[q1] - etaTrd[q2]);
			hdEta->Fill(dEta_smj,mcwt);
			if (numlrj>1) {
				TLorentzVector pj1,pj2,pq1,pq2,pH,pq_lrj,pq_srj,pj_H,pj_nH,pqq,pjj;
				// declare data to write to histo
				double dR_lrji_nq, dR_lrji_fq, dR_qq;
				pH.SetPtEtaPhiM(ptH,etaH,phiH,mH);
				pj1.SetPtEtaPhiM(ptj1,etaj1,phij1,mj1);
				pj2.SetPtEtaPhiM(ptj2,etaj2,phij2,mj2);
				pq1.SetPtEtaPhiM(ptTrd[q1],etaTrd[q1],phiTrd[q1],mTrd[q1]);
				pq2.SetPtEtaPhiM(ptTrd[q2],etaTrd[q2],phiTrd[q2],mTrd[q2]);
				pqq = pq1 + pq2; // di-q system 4-mom
				dR_qq = pq1.DeltaR(pq2);
				if ( pj1.DeltaR(pH) < 1 ) { // which LRJ corresponds to a quark
					pj_H = pj1; pj_nH = pj2; // assign H and non-H LRJs
					dR_lrji_nq = min( pj2.DeltaR(pq1), pj2.DeltaR(pq2) );
					dR_lrji_fq = max( pj2.DeltaR(pq1), pj2.DeltaR(pq2) );
					// assign which quarks are in LRJ and which should be in a SRJ
					if ( pj2.DeltaR(pq1) < pj2.DeltaR(pq2) ) {
						pq_lrj = pq1; pq_srj = pq2; }
					else {
						pq_lrj = pq2; pq_srj = pq1; }
				}
				else if ( pj2.DeltaR(pH) < 1 ) {
					pj_H = pj2; pj_nH = pj1;
					dR_lrji_nq = min( pj1.DeltaR(pq1), pj1.DeltaR(pq2) );
					dR_lrji_fq = max( pj1.DeltaR(pq1), pj1.DeltaR(pq2) );
					if ( pj1.DeltaR(pq1) < pj1.DeltaR(pq2) ) {
						pq_lrj = pq1; pq_srj = pq2; }
					else {
						pq_lrj = pq2; pq_srj = pq1; }
				}
				double wsrj_q = -2; // which SRJ is the q, of all SRJs outside both LRJs
				int nsrj_o_lrj = 0;
				//double dR_cutoff = 1.6; // minimum dR from non-H LRJ to search for other q
				double deta_cutoff = 1.4;
				TLorentzVector psrj,ptest;
				// find which SRJ corresponds to the non-LRJ quark
				// 3/05: changed search region to deta cutoff
				if ( pq_srj.DeltaR(pj1)>1 and pq_srj.DeltaR(pj2)>1 ) {
					for (int j=0; j<ptsrj->size(); j++) {
						ptest.SetPtEtaPhiM(ptsrjd[j],etasrjd[j],phisrjd[j],msrjd[j]);
						if ( ptest.DeltaR(pj_H)>1 and abs(etasrjd[j]-pj_nH.Eta())>deta_cutoff) { 
							if ( ptest.DeltaR(pq_srj)<0.4) {
								psrj = ptest;
								wsrj_q = nsrj_o_lrj;
								break;
							}
							nsrj_o_lrj++;
						}
					}
				}
				// set to -1 if the other quark is hidden in one of the LRJs
				// still -2 if there are no SRJs corresponding to this quark
				else { wsrj_q = -1; }

				// write srj data if we have matched one to non-LRJ q
				// (not necessarily found the correct SRJ)
				double q_srj_eta = abs(pq_srj.Eta());
				double q_srj_pt = pq_srj.Pt();
				double matched_srj_eta = -1;
				double matched_srj_pt = -1;
				double mqq = pqq.M();
				double mjj;
				double dPhiqq = -1;
				if ( wsrj_q > -1 ) {
					pjj = psrj + pj_nH;
					mjj = pjj.M();
					matched_srj_eta = abs(psrj.Eta());
					matched_srj_pt = psrj.Pt();
				}
				dPhiqq = abs(pq1.Phi() - pq2.Phi());
				if (dPhiqq > pi) { dPhiqq = 2*pi - dPhiqq; }

				vector<double> hdata = {dR_lrji_nq, dR_lrji_fq, dR_qq, wsrj_q, q_srj_eta,
							q_srj_pt, matched_srj_eta, matched_srj_pt, mqq, mjj, dPhiqq};
				hlrj.fill(hdata,mcwt);
			}
		}
	}

	TCanvas *c = new TCanvas("c","c",1600,1200);
	c->Divide(2,2);
	int plotnum = 7;
	if (plotnum == 10) {
		c->cd(1); hlrj.draw(10);
	}
	if (plotnum == 9) {
		c->cd(1); hlrj.draw(8);
		hlrj.hv[9]->SetLineColor(kRed); hlrj.hv[9]->DrawCopy("same");
	}
	if (plotnum==7) { // which srj corresponds to q plot
		c->cd(1); hlrj.draw(3);
	}
	if (plotnum==8) {
		c->cd(1); hlrj.draw(4);
		hlrj.hv[6]->SetLineColor(kRed); hlrj.hv[6]->DrawCopy("same");
		c->cd(2); hlrj.draw(5);
		hlrj.hv[7]->SetLineColor(kRed); hlrj.hv[7]->DrawCopy("same");
	}
	if (plotnum==1) {
		c->cd(1);
		hdEta->DrawCopy();
	}
	if (plotnum==5) { // deltaR between non-Higgs LRJ and nearest/furthest quark
		c->cd(1); hlrj.draw(0);
		c->cd(2); hlrj.draw(1);
	}
	if (plotnum==6) { // deltaR between truth quarks
		c->cd(1); hlrj.draw(2);
		c->cd(2); hdEta->DrawCopy();
	}
}
