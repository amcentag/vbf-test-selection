#include <string>
#include "../histo.h"

using namespace std;

void ptm_trigeff() {
	int trignum = 2;
	int varnum = 3; // 1 = jet pT, 2 = jet M, 3 = jet rho
	string trigname, sel;
	if (trignum==1) { trigname = "passHLT_j420_a10_lcw_L1J100"; }
	if (trignum==2) { trigname = "passHLT_j360_a10_lcw_sub_L1J100"; }
	if (trignum==3) { trigname = "passHLT_j440_a10t_lcw_jes_L1J100"; }
	if (trignum==4) { trigname = "passHLT_j460_a10t_lcw_jes_L1J100"; }
	if (trignum==5) { trigname = "passHLT_j390_a10t_lcw_jes_30smcINF_L1J100"; }
	if (trignum==6) { trigname = "passHLT_j420_a10t_lcw_jes_35smcINF_L1J100"; }
	if (trignum==7) { trigname = "passHLT_2j330_a10t_lcw_jes_30smcINF_L1J100"; }
	if (trignum==8) { trigname = "passHLT_j420_a10t_lcw_jes_35smcINF_L1SC111"; }
	sel = "&& Jet1_pT>450 && Jet2_pT>250)*weight";

	unique_ptr<TFile> F(TFile::Open("~/vbf/ntuples/VBF.root"));
	auto tree = F->Get<TTree>("Nominal");

	double pth = 0;
	tree->SetBranchAddress("HCand_pT",&pth);
	double mh = 0;
	tree->SetBranchAddress("HCand_M",&mh);
	int trig = 0;
	tree->SetBranchAddress(trigname.c_str(),&trig);
	double mcwt = 0;
	tree->SetBranchAddress("weight", &mcwt);
	double ptj1 = 0;
	tree->SetBranchAddress("Jet1_pT", &ptj1);
	double ptj2 = 0;
	tree->SetBranchAddress("Jet2_pT", &ptj2);

	TH1D* hraw = new TH1D("hraw","VBF rho",30,-4,-1);
	TH1D* hsel = new TH1D("hsel","VBF rho with selection",30,-4,-1);
	TH1D* htrig = new TH1D("htrig","VBF rho with selection & trigger",30,-4,-1);

	for (int i=0; i<tree->GetEntries(); i++) {
		tree->GetEntry(i);
		double rho = log(pow(mh,2)/pow(pth,2));
		hraw->Fill(rho,mcwt);
		if (ptj1>450 and ptj2>250) {
			hsel->Fill(rho,mcwt);
			if (trig) { htrig->Fill(rho,mcwt); }
		}
	}

	TCanvas *c1 = new TCanvas("c1","c1",1600,800);
	c1->Divide(2,1);

	c1->cd(1);
	if (varnum==1) {
		tree->SetLineColor(kBlue);
		tree->Draw("Jet1_pT",("(1"+sel).c_str());
		tree->SetLineColor(kRed);
		tree->Draw("Jet1_pT",("("+trigname+sel).c_str(),"same");
	}
	if (varnum==2) {
		tree->SetLineColor(kBlue);
		tree->Draw("Jet1_M",("(1"+sel).c_str());
		tree->SetLineColor(kRed);
		tree->Draw("Jet1_M",("("+trigname+sel).c_str(),"same");
	}
	if (varnum==3) {
		hsel->SetLineColor(kBlue);
		hsel->DrawCopy();
		htrig->SetLineColor(kRed);
		htrig->DrawCopy("same");
	}

	c1->cd(2);
	if (varnum==1) {
		tree->SetLineColor(kBlue);
		tree->Draw("Jet2_pT",("(1"+sel).c_str());
		tree->SetLineColor(kRed);
		tree->Draw("Jet2_pT",("("+trigname+sel).c_str(),"same");
	}
	if (varnum==2) {
		tree->SetLineColor(kBlue);
		tree->Draw("Jet2_M",("(1"+sel).c_str());
		tree->SetLineColor(kRed);
		tree->Draw("Jet2_M",("("+trigname+sel).c_str(),"same");
	}

}
