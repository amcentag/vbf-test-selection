using namespace std;

// things to clean up the code:
//
// - start writing/reading all trees from just one file
void plotsmjhistos() {
	// VBF trees
	unique_ptr<TFile> fvbf(TFile::Open("~/vbf/test-selection/VBFsmj.root"));
	unique_ptr<TFile> ntuplevbf(TFile::Open("~/vbf/ntuples/VBF.root"));
	auto treevbf = ntuplevbf->Get<TTree>("Nominal");
	// trees w/o selection
	unique_ptr<TH1D> hvbf_ptH(fvbf->Get<TH1D>("ptH_all_VBF"));
	unique_ptr<TH1D> hvbf_ptH_1lrj(fvbf->Get<TH1D>("ptH_1lrj_VBF"));
	unique_ptr<TH1D> hvbf_srj_dR(fvbf->Get<TH1D>("2srj_delR_1lrj_VBF"));
	unique_ptr<TH1D> hvbf_srj_M(fvbf->Get<TH1D>("2srj_M_1lrj_VBF"));
	unique_ptr<TH1D> hvbf_lrj_dR(fvbf->Get<TH1D>("2lrj_delR_VBF"));
	unique_ptr<TH1D> hvbf_2lrj_njo(fvbf->Get<TH1D>("2lrj_njets_olrj_VBF"));
	unique_ptr<TH1D> hvbf_ptH_dRlt16(fvbf->Get<TH1D>("ptH_dR<1.6_VBF"));
	unique_ptr<TH1D> hvbf_ptH_dRgt16(fvbf->Get<TH1D>("ptH_dR>1.6_VBF"));
	// trees w/ selection ptj1 > 450 GeV
	unique_ptr<TH1D> hvbf_ptH_sel(fvbf->Get<TH1D>("ptH_all_VBF_sel"));
	unique_ptr<TH1D> hvbf_ptH_1lrj_sel(fvbf->Get<TH1D>("ptH_1lrj_VBF_sel"));
	unique_ptr<TH1D> hvbf_srj_dR_sel(fvbf->Get<TH1D>("2srj_delR_1lrj_VBF_sel"));
	unique_ptr<TH1D> hvbf_srj_M_sel(fvbf->Get<TH1D>("2srj_M_1lrj_VBF_sel"));
	unique_ptr<TH1D> hvbf_lrj_dR_sel(fvbf->Get<TH1D>("2lrj_delR_VBF_sel"));
	unique_ptr<TH1D> hvbf_2lrj_njo_sel(fvbf->Get<TH1D>("2lrj_njets_olrj_VBF_sel"));
	unique_ptr<TH1D> hvbf_H_wj_sel(fvbf->Get<TH1D>("H_whichj_VBF_sel"));
	 
	// VH trees
	unique_ptr<TFile> fvh(TFile::Open("~/vbf/test-selection/VHsmj.root"));
	unique_ptr<TFile> ntuplevh(TFile::Open("~/vbf/ntuples/VH.root"));
	auto treevh = ntuplevh->Get<TTree>("Nominal");
	// trees w/o selection
	unique_ptr<TH1D> hvh_ptH(fvh->Get<TH1D>("ptH_all_VH"));
	unique_ptr<TH1D> hvh_ptH_1lrj(fvh->Get<TH1D>("ptH_1lrj_VH"));
	unique_ptr<TH1D> hvh_srj_dR(fvh->Get<TH1D>("2srj_delR_1lrj_VH"));
	unique_ptr<TH1D> hvh_srj_M(fvh->Get<TH1D>("2srj_M_1lrj_VH"));
	unique_ptr<TH1D> hvh_lrj_dR(fvh->Get<TH1D>("2lrj_delR_VH"));
	unique_ptr<TH1D> hvh_2lrj_njo(fvh->Get<TH1D>("2lrj_njets_olrj_VH"));
	unique_ptr<TH1D> hvh_ptH_dRlt16(fvh->Get<TH1D>("ptH_dR<1.6_VH"));
	unique_ptr<TH1D> hvh_ptH_dRgt16(fvh->Get<TH1D>("ptH_dR>1.6_VH"));
	// trees w/ selection ptj1 > 450 GeV
	unique_ptr<TH1D> hvh_ptH_sel(fvh->Get<TH1D>("ptH_all_VH_sel"));
	unique_ptr<TH1D> hvh_ptH_1lrj_sel(fvh->Get<TH1D>("ptH_1lrj_VH_sel"));
	unique_ptr<TH1D> hvh_srj_dR_sel(fvh->Get<TH1D>("2srj_delR_1lrj_VH_sel"));
	unique_ptr<TH1D> hvh_srj_M_sel(fvh->Get<TH1D>("2srj_M_1lrj_VH_sel"));
	unique_ptr<TH1D> hvh_lrj_dR_sel(fvh->Get<TH1D>("2lrj_delR_VH_sel"));
	unique_ptr<TH1D> hvh_2lrj_njo_sel(fvh->Get<TH1D>("2lrj_njets_olrj_VH_sel"));
	unique_ptr<TH1D> hvh_H_wj_sel(fvh->Get<TH1D>("H_whichj_VH_sel"));
	 
	// ggH trees
	unique_ptr<TFile> fggh(TFile::Open("~/vbf/test-selection/ggHsmj.root"));
	unique_ptr<TFile> ntupleggh(TFile::Open("~/vbf/ntuples/ggH.root"));
	auto treeggh = ntupleggh->Get<TTree>("Nominal");
	// trees w/o selection
	unique_ptr<TH1D> hggh_ptH(fggh->Get<TH1D>("ptH_all_ggH"));
	unique_ptr<TH1D> hggh_ptH_1lrj(fggh->Get<TH1D>("ptH_1lrj_ggH"));
	unique_ptr<TH1D> hggh_srj_dR(fggh->Get<TH1D>("2srj_delR_1lrj_ggH"));
	unique_ptr<TH1D> hggh_srj_M(fggh->Get<TH1D>("2srj_M_1lrj_ggH"));
	unique_ptr<TH1D> hggh_lrj_dR(fggh->Get<TH1D>("2lrj_delR_ggH"));
	unique_ptr<TH1D> hggh_2lrj_njo(fggh->Get<TH1D>("2lrj_njets_olrj_ggH"));
	unique_ptr<TH1D> hggh_ptH_dRlt16(fggh->Get<TH1D>("ptH_dR<1.6_ggH"));
	unique_ptr<TH1D> hggh_ptH_dRgt16(fggh->Get<TH1D>("ptH_dR>1.6_ggH"));
	// trees w/ selection ptj1 > 450 GeV
	unique_ptr<TH1D> hggh_ptH_sel(fggh->Get<TH1D>("ptH_all_ggH_sel"));
	unique_ptr<TH1D> hggh_ptH_1lrj_sel(fggh->Get<TH1D>("ptH_1lrj_ggH_sel"));
	unique_ptr<TH1D> hggh_srj_dR_sel(fggh->Get<TH1D>("2srj_delR_1lrj_ggH_sel"));
	unique_ptr<TH1D> hggh_srj_M_sel(fggh->Get<TH1D>("2srj_M_1lrj_ggH_sel"));
	unique_ptr<TH1D> hggh_lrj_dR_sel(fggh->Get<TH1D>("2lrj_delR_ggH_sel"));
	unique_ptr<TH1D> hggh_2lrj_njo_sel(fggh->Get<TH1D>("2lrj_njets_olrj_ggH_sel"));
	unique_ptr<TH1D> hggh_H_wj_sel(fggh->Get<TH1D>("H_whichj_ggH_sel"));
	 
	// ttH trees
	unique_ptr<TFile> ftth(TFile::Open("~/vbf/test-selection/ttHsmj.root"));
	unique_ptr<TFile> ntupletth(TFile::Open("~/vbf/ntuples/ttH.root"));
	auto treetth = ntupletth->Get<TTree>("Nominal");
	// trees w/o selection
	unique_ptr<TH1D> htth_ptH(ftth->Get<TH1D>("ptH_all_ttH"));
	unique_ptr<TH1D> htth_ptH_1lrj(ftth->Get<TH1D>("ptH_1lrj_ttH"));
	unique_ptr<TH1D> htth_srj_dR(ftth->Get<TH1D>("2srj_delR_1lrj_ttH"));
	unique_ptr<TH1D> htth_srj_M(ftth->Get<TH1D>("2srj_M_1lrj_ttH"));
	unique_ptr<TH1D> htth_lrj_dR(ftth->Get<TH1D>("2lrj_delR_ttH"));
	unique_ptr<TH1D> htth_2lrj_njo(ftth->Get<TH1D>("2lrj_njets_olrj_ttH"));
	unique_ptr<TH1D> htth_ptH_dRlt16(ftth->Get<TH1D>("ptH_dR<1.6_ttH"));
	unique_ptr<TH1D> htth_ptH_dRgt16(ftth->Get<TH1D>("ptH_dR>1.6_ttH"));
	// trees w/ selection ptj1 > 450 GeV
	unique_ptr<TH1D> htth_ptH_sel(ftth->Get<TH1D>("ptH_all_ttH_sel"));
	unique_ptr<TH1D> htth_ptH_1lrj_sel(ftth->Get<TH1D>("ptH_1lrj_ttH_sel"));
	unique_ptr<TH1D> htth_srj_dR_sel(ftth->Get<TH1D>("2srj_delR_1lrj_ttH_sel"));
	unique_ptr<TH1D> htth_srj_M_sel(ftth->Get<TH1D>("2srj_M_1lrj_ttH_sel"));
	unique_ptr<TH1D> htth_lrj_dR_sel(ftth->Get<TH1D>("2lrj_delR_ttH_sel"));
	unique_ptr<TH1D> htth_2lrj_njo_sel(ftth->Get<TH1D>("2lrj_njets_olrj_ttH_sel"));
	unique_ptr<TH1D> htth_H_wj_sel(ftth->Get<TH1D>("H_whichj_ttH_sel"));

	// 0: deltaR for 1lrj events, 1: mjj outside H, 2: deltaR between 2lrj
	// 3: fraction of 1 largeR jet events against ptH
	// 4: # SRJ outside both LRJ for events with 2+ LRJ
	// 5: ptH for events with 1 LRJ, deltaR between SRJ < 1.6 and > 1.6
	int plotnum = 0;

	if (plotnum==5) {
		TCanvas *c = new TCanvas("c","c",1600,1200);
		c->Divide(2,2);

		c->cd(1);
		hvbf_ptH_dRlt16->SetTitle("VBF, 1 LRJ, leading SRJ dR < or > 1.6");
		hvbf_ptH_dRlt16->GetXaxis()->SetTitle("ptH");
		hvbf_ptH_dRlt16->DrawCopy();
		hvbf_ptH_dRgt16->SetLineColor(kRed);
		hvbf_ptH_dRgt16->DrawCopy("same");
		c->cd(2);
		hvh_ptH_dRlt16->SetTitle("VH, 1 LRJ, leading SRJ dR < or > 1.6");
		hvh_ptH_dRlt16->GetXaxis()->SetTitle("ptH");
		hvh_ptH_dRlt16->DrawCopy();
		hvh_ptH_dRgt16->SetLineColor(kRed);
		hvh_ptH_dRgt16->DrawCopy("same");
		c->cd(3);
		hggh_ptH_dRlt16->SetTitle("ggH, 1 LRJ, leading SRJ dR < or > 1.6");
		hggh_ptH_dRlt16->GetXaxis()->SetTitle("ptH");
		hggh_ptH_dRlt16->DrawCopy();
		hggh_ptH_dRgt16->SetLineColor(kRed);
		hggh_ptH_dRgt16->DrawCopy("same");
		c->cd(4);
		htth_ptH_dRlt16->SetTitle("ttH, 1 LRJ, leading SRJ dR < or > 1.6");
		htth_ptH_dRlt16->GetXaxis()->SetTitle("ptH");
		htth_ptH_dRlt16->DrawCopy();
		htth_ptH_dRgt16->SetLineColor(kRed);
		htth_ptH_dRgt16->DrawCopy("same");
	}

	if (plotnum==4) {
		TCanvas *c = new TCanvas("c","c",1600,1200);
		c->Divide(2,2);

		c->cd(1);
		hvbf_2lrj_njo->SetTitle("VBF, # SRjets outside both LRjets");
		hvbf_2lrj_njo->DrawCopy();
		c->cd(2);
		hvh_2lrj_njo->SetTitle("VH, # SRjets outside both LRjets");
		hvh_2lrj_njo->DrawCopy();
		c->cd(3);
		hvbf_2lrj_njo_sel->SetLineColor(kRed);
		hvbf_2lrj_njo_sel->SetTitle("VBF, # SRjets outside both LRjets, ptj1>450");
		hvbf_2lrj_njo_sel->DrawCopy();
		c->cd(4);
		hvh_2lrj_njo_sel->SetLineColor(kRed);
		hvh_2lrj_njo_sel->SetTitle("VH, # SRjets outside both LRjets, ptj1>450");
		hvh_2lrj_njo_sel->DrawCopy();
	}

	if (plotnum==3) {
		TCanvas *c = new TCanvas("c","c",1600,1200);
		c->Divide(2,2);
		// create histos to divide for ratio
		TH1D *hvbf1 = new TH1D("hvbf1","hvbf_ptH",30,250,1000);
		hvbf1 = (TH1D*)hvbf_ptH->Clone();
		TH1D *hvh1 = new TH1D("hvh1","hvh_ptH",30,250,1000);
		hvh1 = (TH1D*)hvh_ptH->Clone();
		TH1D *hggh1 = new TH1D("hggh1","hggh_ptH",30,250,1000);
		hggh1 = (TH1D*)hggh_ptH->Clone();
		TH1D *htth1 = new TH1D("htth1","htth_ptH",30,250,1000);
		htth1 = (TH1D*)htth_ptH->Clone();
		TH1D *hvbf2 = new TH1D("hvbf2","hvbf_ptH_sel",30,250,1000);
		hvbf2 = (TH1D*)hvbf_ptH_sel->Clone();
		TH1D *hvh2 = new TH1D("hvh2","hvh_ptH_sel",30,250,1000);
		hvh2 = (TH1D*)hvh_ptH_sel->Clone();
		TH1D *hggh2 = new TH1D("hggh2","hggh_ptH_sel",30,250,1000);
		hggh2 = (TH1D*)hggh_ptH_sel->Clone();
		TH1D *htth2 = new TH1D("htth2","htth_ptH_sel",30,250,1000);
		htth2 = (TH1D*)htth_ptH_sel->Clone();

		hvbf_ptH_1lrj->Divide(hvbf1);
		hvh_ptH_1lrj->Divide(hvh1);
		hggh_ptH_1lrj->Divide(hggh1);
		htth_ptH_1lrj->Divide(htth1);
		hvbf_ptH_1lrj_sel->Divide(hvbf2);
		hvh_ptH_1lrj_sel->Divide(hvh2);
		hggh_ptH_1lrj_sel->Divide(hggh2);
		htth_ptH_1lrj_sel->Divide(htth2);

		c->cd(1);
		hvbf_ptH_1lrj->SetTitle("VBF 1 Large-R Jet Event Fraction");
		hvbf_ptH_1lrj->GetXaxis()->SetTitle("ptH");
		hvbf_ptH_1lrj->GetYaxis()->SetRangeUser(0,1);
		hvbf_ptH_1lrj->DrawCopy();
		hvbf_ptH_1lrj_sel->SetLineColor(kRed);
		hvbf_ptH_1lrj_sel->DrawCopy("same");
		c->cd(2);
		hvh_ptH_1lrj->SetTitle("VH 1 Large-R Jet Event Fraction");
		hvh_ptH_1lrj->GetXaxis()->SetTitle("ptH");
		hvh_ptH_1lrj->GetYaxis()->SetRangeUser(0,1);
		hvh_ptH_1lrj->DrawCopy();
		hvh_ptH_1lrj_sel->SetLineColor(kRed);
		hvh_ptH_1lrj_sel->DrawCopy("same");
		c->cd(3);
		hggh_ptH_1lrj->SetTitle("ggH 1 Large-R Jet Event Fraction");
		hggh_ptH_1lrj->GetXaxis()->SetTitle("ptH");
		hggh_ptH_1lrj->GetYaxis()->SetRangeUser(0,1);
		hggh_ptH_1lrj->DrawCopy();
		hggh_ptH_1lrj_sel->SetLineColor(kRed);
		hggh_ptH_1lrj_sel->DrawCopy("same");
		c->cd(4);
		htth_ptH_1lrj->SetTitle("ttH 1 Large-R Jet Event Fraction");
		htth_ptH_1lrj->GetXaxis()->SetTitle("ptH");
		htth_ptH_1lrj->GetYaxis()->SetRangeUser(0,1);
		htth_ptH_1lrj->DrawCopy();
		htth_ptH_1lrj_sel->SetLineColor(kRed);
		htth_ptH_1lrj_sel->DrawCopy("same");
	}

	if (plotnum==0) {
		TCanvas *c = new TCanvas("c","c",1600,1200);
		c->Divide(2,2);
		int withsel = 0;

		if (withsel==0) {
			c->cd(1);
			hvbf_srj_dR->SetTitle("VBF deltaR between 2 leading SRJ outside H");
			hvbf_srj_dR->DrawCopy();
			c->cd(2);
			hvh_srj_dR->SetTitle("VH deltaR between 2 leading SRJ outside H");
			hvh_srj_dR->DrawCopy();
			c->cd(3);
			hggh_srj_dR->SetTitle("ggH deltaR between 2 leading SRJ outside H");
			hggh_srj_dR->DrawCopy();
			c->cd(4);
			htth_srj_dR->SetTitle("ttH deltaR between 2 leading SRJ outside H");
			htth_srj_dR->DrawCopy();
		}

		if (withsel==1) {
			c->cd(1);
			hvbf_srj_dR_sel->SetTitle("VBF deltaR between 2 leading SRJ outside H");
			hvbf_srj_dR_sel->DrawCopy();
			c->cd(2);
			hvh_srj_dR_sel->SetTitle("VH deltaR between 2 leading SRJ outside H");
			hvh_srj_dR_sel->DrawCopy();
			c->cd(3);
			hggh_srj_dR_sel->SetTitle("ggH deltaR between 2 leading SRJ outside H");
			hggh_srj_dR_sel->DrawCopy();
			c->cd(4);
			htth_srj_dR_sel->SetTitle("ttH deltaR between 2 leading SRJ outside H");
			htth_srj_dR_sel->DrawCopy();
		}
	}

	if (plotnum==1) {
		TCanvas *c = new TCanvas("c","c",1600,1200);
		c->Divide(2,2);
		int withsel = 1;

		if (withsel==0) {
		c->cd(1);
		hvbf_srj_M->DrawCopy();
		c->cd(2);
		hvh_srj_M->DrawCopy();
		c->cd(3);
		hggh_srj_M->DrawCopy();
		c->cd(4);
		htth_srj_M->DrawCopy();
		}

		if (withsel==1) {
			c->cd(1);
			hvbf_srj_M_sel->SetTitle("VBF inv mass of leading jet(s) outside H");
			hvbf_srj_M_sel->DrawCopy();
			c->cd(2);
			hvh_srj_M_sel->SetTitle("VH inv mass of leading jet(s) outside H");
			hvh_srj_M_sel->DrawCopy();
			c->cd(3);
			hggh_srj_M_sel->SetTitle("ggH inv mass of leading jet(s) outside H");
			hggh_srj_M_sel->DrawCopy();
			c->cd(4);
			htth_srj_M_sel->SetTitle("ttH inv mass of leading jet(s) outside H");
			htth_srj_M_sel->DrawCopy();
		}
	}

	if (plotnum==2) {
		TCanvas *c = new TCanvas ("c","c",1600,1200);
		c->Divide(2,2);

		c->cd(1);
		hvbf_lrj_dR->DrawCopy();
		c->cd(2);
		hvh_lrj_dR->DrawCopy();
		c->cd(3);
		hggh_lrj_dR->DrawCopy();
		c->cd(4);
		htth_lrj_dR->DrawCopy();
	}
}
