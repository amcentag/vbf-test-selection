#include "../histo.h"
#include <string>
#include <cstring>

using namespace std;

const double pi = 3.14159265359;

// plot small-R jet kinematics for various H production modes
// we already have a branch for SmallRJets_deltaRH so let's just find the
// deltaR between two leading small jets
// inv mass of two leading small jets
// look at these only if numLargeRJets = 1, if there are more then check
// kinematics between the large R jets
void smjhistos() {
	// procnames: VBF, VH, ggH, ttH
	string procname = "ggH";
	cout << "Running small jet kinematics for " << procname << endl;
	unique_ptr<TFile> f(TFile::Open(("~/vbf/ntuples/"+procname+".root").c_str()));
	auto tree = f->Get<TTree>("Nominal");

	double mcwt=0;
	tree->SetBranchAddress("weight",&mcwt);
	double ptH=0;
	tree->SetBranchAddress("HCand_pT",&ptH);
	double etaH=0;
	tree->SetBranchAddress("HCand_Eta",&etaH);
	double phiH=0;
	tree->SetBranchAddress("HCand_Phi",&phiH);
	double mH=0;
	tree->SetBranchAddress("HCand_M",&mH);
	vector<double> *ptsmj=0;
	tree->SetBranchAddress("SmallRJets_pT",&ptsmj);
	vector<double> *etasmj=0;
	tree->SetBranchAddress("SmallRJets_eta",&etasmj);
	vector<double> *phismj=0;
	tree->SetBranchAddress("SmallRJets_phi",&phismj);
	vector<double> *msmj=0;
	tree->SetBranchAddress("SmallRJets_M",&msmj);
	double ptj1=0;
	tree->SetBranchAddress("Jet1_pT",&ptj1);
	double etaj1=0;
	tree->SetBranchAddress("Jet1_eta",&etaj1);
	double phij1=0;
	tree->SetBranchAddress("Jet1_phi",&phij1);
	double mj1=0;
	tree->SetBranchAddress("Jet1_M",&mj1);
	double ptj2=0;
	tree->SetBranchAddress("Jet2_pT",&ptj2);
	double etaj2=0;
	tree->SetBranchAddress("Jet2_eta",&etaj2);
	double phij2=0;
	tree->SetBranchAddress("Jet2_phi",&phij2);
	double mj2=0;
	tree->SetBranchAddress("Jet2_M",&mj2);
	int numlrj=0;
	tree->SetBranchAddress("nLargeRJets",&numlrj);

	/*
	vector<string> histonames={"ptH_all","ptH_1lrj","2srj_delR_1lrj","2srj_M_1lrj","2lrj_delR",
								"2lrj_njets_olrj","ptH_dR<1.6","ptH_dR>1.6","H_whichj"};
	vector<double> histobins={30,250,1000,30,250,1000,24,0,2*pi,50,0,1000,24,0,2*pi,8,-0.5,7.5,
								25,250,750,25,250,750,2,0.5,2.5};
	*/
	vector<string> histonames = {"hdRjj", "hdEtajj", "hmjj", "hdPhijj"};
	vector<string> histotitles = {"deltaR b/w selected q-jets", "deltaEta b/w selected q-jets",
			"Mjj of selected q-jets", "deltaPhi b/w selected q-jets"};
	vector<double> histobins = {40,0,8, 40,0,8, 30,0,3000, 24,0,pi};
	histo hsel(histonames, histotitles, procname.c_str(), histobins);
	//histo hraw(histonames, procname.c_str(), histobins);
	//histo hsel(histonames, (procname+"_sel").c_str(), histobins);

	for (int i=0; i<tree->GetEntries(); i++) {
		tree->GetEntry(i);
		TLorentzVector pH;
		pH.SetPtEtaPhiM(ptH,etaH,phiH,mH);
		/*
		if (numlrj==1) {
			int njets_oH=0;
			double smj_dR_oH=-1;
			double smj_Mjj_oH=-1;
			double ptH_dR_lt16=0;
			double ptH_dR_gt16=0;
			double *ptsmjd = ptsmj->data();
			double *etasmjd = etasmj->data();
			double *phismjd = phismj->data();
			double *msmjd = msmj->data();
			TLorentzVector pj1,pj2,pjtest;
			for (int j=0; j<ptsmj->size(); j++) {
				pjtest.SetPtEtaPhiM(ptsmjd[j],etasmjd[j],phismjd[j],msmjd[j]);
				double deltaRH = pjtest.DeltaR(pH);
				bool thisjet_oH = false;
				if (deltaRH>1.0) { njets_oH++; thisjet_oH = true; }
				// cout << "entry " << i << ", jet " << j << ", dR = " << deltaRH << ", njets_oH = " << 
				//		njets_oH << endl;
				if (njets_oH==1 and thisjet_oH) {
					pj1.SetPtEtaPhiM(ptsmjd[j],etasmjd[j],phismjd[j],msmjd[j]); }
				if (njets_oH==2 and thisjet_oH) {
					pj2.SetPtEtaPhiM(ptsmjd[j],etasmjd[j],phismjd[j],msmjd[j]); }
			}
			if (njets_oH>1) {
				TLorentzVector pj12 = pj1 + pj2;
				smj_Mjj_oH = pj12.M();
				smj_dR_oH = pj1.DeltaR(pj2);
				if (smj_dR_oH < 1.6) { ptH_dR_lt16 = ptH; }
				else { ptH_dR_gt16 = ptH; }
				// cout << "selected jet 1: dRH = " << pj1.DeltaR(pH) << ", eta = " << pj1.Eta() << 
				//	 	", phi = " << pj1.Phi() << endl;
				//cout << "selected jet 2: dRH = " << pj2.DeltaR(pH) << ", eta = " << pj2.Eta() << 
				//		", phi = " << pj2.Phi() << ", dRJ1 = " << smj_dR_oH << endl << endl;
			}
			vector<double> histodata = {ptH, ptH, smj_dR_oH, smj_Mjj_oH, -1.0, -1.0, ptH_dR_lt16,
				ptH_dR_gt16,0};
			hraw.fill(histodata,mcwt);
			if (ptj1>450) { hsel.fill(histodata,mcwt); }
		}
		*/
		if (numlrj>1) {
			TLorentzVector pj1,pj2,pjtest,pj_H,pj_nH,psrj_q;
			pj1.SetPtEtaPhiM(ptj1,etaj1,phij1,mj1);
			pj2.SetPtEtaPhiM(ptj2,etaj2,phij2,mj2);
			// assign H and non-H LRJs
			if ( pj1.DeltaR(pH) < 1 ) { pj_H = pj1; pj_nH = pj2; }
			else if ( pj2.DeltaR(pH) < 1 ) { pj_H = pj2; pj_nH = pj1; }
			double *ptsmjd = ptsmj->data();
			double *etasmjd = etasmj->data();
			double *phismjd = phismj->data();
			double *msmjd = msmj->data();
			bool qjet_found = 0; // set true if we find a SRJ in selection region
			// find srj in selection region that probably is the other quark
			// selection region: dR>1 from H LRJ, dR>1.6 from non-H LRJ
			// 3/05: changed sel region to dEta>1.5 from non-H LRJ
			for (int j=0; j<ptsmj->size(); j++) {
				pjtest.SetPtEtaPhiM(ptsmjd[j],etasmjd[j],phismjd[j],msmjd[j]);
				if ( pjtest.DeltaR(pj_H)>1 and abs(etasmjd[j]-pj_nH.Eta())>1.5 and ptsmjd[j]>25 ) {
					psrj_q = pjtest;
					qjet_found = 1;
					break;
				}
			}
			if (qjet_found) {
				double dRjj = pj_nH.DeltaR(psrj_q); 
				double dEtajj = abs( pj_nH.Eta() - psrj_q.Eta() );
				double mjj = (pj_nH + psrj_q).M();
				double dPhijj = abs( pj_nH.Phi() - psrj_q.Phi() );
				if (dPhijj > pi) { dPhijj = 2*pi - dPhijj; }
				vector<double> histodata = {dRjj, dEtajj, mjj, dPhijj};
				if (ptj1>450) { hsel.fill(histodata,mcwt); }
			}
		}
	}

	delete ptsmj;
	delete etasmj;
	delete phismj;
	delete msmj;

	/*
	unique_ptr<TFile> outF(TFile::Open(
		("~/vbf/test-selection/"+procname+"smj.root").c_str(),"RECREATE" ));
	*/
	unique_ptr<TFile> outF(TFile::Open("~/vbf/test-selection/outhistos_CXAOD_new.root","update"));
	outF->cd();
	hsel.write();
	outF->Close();
}
