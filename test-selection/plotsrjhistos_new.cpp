// code to make plots written from smjhistos.cpp to outhistos_CXAOD_new.root
//

using namespace std;

void plotsrjhistos_new() {
	unique_ptr<TFile> fh(TFile::Open("outhistos_CXAOD_new.root","read"));

	unique_ptr<TH1D> hdRjj_VH(fh->Get<TH1D>("hdRjj_VH"));
	unique_ptr<TH1D> hdEtajj_VH(fh->Get<TH1D>("hdEtajj_VH"));
	unique_ptr<TH1D> hmjj_VH(fh->Get<TH1D>("hmjj_VH"));
	unique_ptr<TH1D> hdPhijj_VH(fh->Get<TH1D>("hdPhijj_VH"));

	unique_ptr<TH1D> hdRjj_VBF(fh->Get<TH1D>("hdRjj_VBF"));
	unique_ptr<TH1D> hdEtajj_VBF(fh->Get<TH1D>("hdEtajj_VBF"));
	unique_ptr<TH1D> hmjj_VBF(fh->Get<TH1D>("hmjj_VBF"));
	unique_ptr<TH1D> hdPhijj_VBF(fh->Get<TH1D>("hdPhijj_VBF"));

	unique_ptr<TH1D> hdRjj_ggH(fh->Get<TH1D>("hdRjj_ggH"));
	unique_ptr<TH1D> hdEtajj_ggH(fh->Get<TH1D>("hdEtajj_ggH"));
	unique_ptr<TH1D> hmjj_ggH(fh->Get<TH1D>("hmjj_ggH"));
	unique_ptr<TH1D> hdPhijj_ggH(fh->Get<TH1D>("hdPhijj_ggH"));
	
	unique_ptr<TH1D> hdRjj_ttH(fh->Get<TH1D>("hdRjj_ttH"));
	unique_ptr<TH1D> hdEtajj_ttH(fh->Get<TH1D>("hdEtajj_ttH"));
	unique_ptr<TH1D> hmjj_ttH(fh->Get<TH1D>("hmjj_ttH"));
	unique_ptr<TH1D> hdPhijj_ttH(fh->Get<TH1D>("hdPhijj_ttH"));

	// 0: dR, dEta between selected q-jets
	// 1: inv mass of selected q-jets system
	// 2: dPhi b/w selected q-jets
	int plotnum = 0;
	TCanvas *c = new TCanvas("c","kinematic plots",1600,1200);
	c->Divide(2,2);

	if (plotnum == 0) {
		c->cd(1); hdRjj_VBF->DrawCopy();
		hdEtajj_VBF->SetLineColor(kRed); hdEtajj_VBF->DrawCopy("same");
		c->cd(2); hdRjj_VH->DrawCopy();
		hdEtajj_VH->SetLineColor(kRed); hdEtajj_VH->DrawCopy("same");
		c->cd(3); hdRjj_ggH->DrawCopy();
		hdEtajj_ggH->SetLineColor(kRed); hdEtajj_ggH->DrawCopy("same");
		c->cd(4); hdRjj_ttH->DrawCopy();
		hdEtajj_ttH->SetLineColor(kRed); hdEtajj_ttH->DrawCopy("same");
	}
	if (plotnum == 1) {
		c->cd(1); hmjj_VBF->DrawCopy();
		c->cd(2); hmjj_VH->DrawCopy();
		c->cd(3); hmjj_ggH->DrawCopy();
		c->cd(4); hmjj_ttH->DrawCopy();
	}
	if (plotnum == 2) {
		c->cd(1); hdPhijj_VBF->DrawCopy();
		c->cd(2); hdPhijj_VH->DrawCopy();
		c->cd(3); hdPhijj_ggH->DrawCopy();
		c->cd(4); hdPhijj_ttH->DrawCopy();
	}
}
