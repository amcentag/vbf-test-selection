// plot efficiency and purity output created by eff_pur_out.cpp
//

using namespace std;

void plot_eff_pur() {
	int plotnum = 0;
	if (plotnum == 0) {
		auto mg1 = new TMultiGraph();
		auto mg2 = new TMultiGraph();
		auto eff10 = new TGraph("data_eff_pur_1000.csv","%lg %lg",",");
		auto eff12 = new TGraph("data_eff_pur_1200.csv","%lg %lg",",");
		auto eff15 = new TGraph("data_eff_pur_1500.csv","%lg %lg",",");
		auto pur10 = new TGraph("data_eff_pur_1000.csv","%lg %*lg %lg",",");
		auto pur12 = new TGraph("data_eff_pur_1200.csv","%lg %*lg %lg",",");
		auto pur15 = new TGraph("data_eff_pur_1500.csv","%lg %*lg %lg",",");

		eff10->SetLineColor(2); eff10->SetMarkerColor(2); eff10->SetMarkerStyle(2);
		pur10->SetLineColor(2); pur10->SetMarkerColor(2); pur10->SetMarkerStyle(2);
		eff12->SetLineColor(3); eff12->SetMarkerColor(3); eff12->SetMarkerStyle(2);
		pur12->SetLineColor(3); pur12->SetMarkerColor(3); pur12->SetMarkerStyle(2);
		eff15->SetLineColor(4); eff15->SetMarkerColor(4); eff15->SetMarkerStyle(2);
		pur15->SetLineColor(4); pur15->SetMarkerColor(4); pur15->SetMarkerStyle(2);
		
		eff10->SetTitle("mjj > 1000"); pur10->SetTitle("mjj > 1000");
		eff12->SetTitle("mjj > 1200"); pur12->SetTitle("mjj > 1200");
		eff15->SetTitle("mjj > 1500"); pur15->SetTitle("mjj > 1500");
		
		mg1->SetTitle("Efficiency; dEta_cutoff;");
		mg1->Add(eff10,"PL"); mg1->Add(eff12,"PL"); mg1->Add(eff15,"PL");
		mg2->SetTitle("Purity; dEta_cutoff;");
		mg2->Add(pur10,"PL"); mg2->Add(pur12,"PL"); mg2->Add(pur15,"PL");

		TCanvas *c = new TCanvas("c","efficiency and purity plots",800,600);
		mg1->Draw("ALP");
		c->BuildLegend();
		//mg2->Draw("ALP");
		//c->BuildLegend();
	}
	if (plotnum == 1) {
		auto mg1 = new TMultiGraph();
		auto mg2 = new TMultiGraph();
		auto eff1 = new TGraph("data_eff_pur_30.csv","%lg %lg",",");
		auto eff2 = new TGraph("data_eff_pur_35.csv","%lg %lg",",");
		auto eff3 = new TGraph("data_eff_pur_40.csv","%lg %lg",",");
		auto pur1 = new TGraph("data_eff_pur_30.csv","%lg %*lg %lg",",");
		auto pur2 = new TGraph("data_eff_pur_35.csv","%lg %*lg %lg",",");
		auto pur3 = new TGraph("data_eff_pur_40.csv","%lg %*lg %lg",",");

		eff1->SetLineColor(2); eff1->SetMarkerColor(2); eff1->SetMarkerStyle(2);
		pur1->SetLineColor(2); pur1->SetMarkerColor(2); pur1->SetMarkerStyle(2);
		eff2->SetLineColor(3); eff2->SetMarkerColor(3); eff2->SetMarkerStyle(2);
		pur2->SetLineColor(3); pur2->SetMarkerColor(3); pur2->SetMarkerStyle(2);
		eff3->SetLineColor(4); eff3->SetMarkerColor(4); eff3->SetMarkerStyle(2);
		pur3->SetLineColor(4); pur3->SetMarkerColor(4); pur3->SetMarkerStyle(2);

		eff1->SetTitle("dEtajj > 3.0"); pur1->SetTitle("dEtajj > 3.0");
		eff2->SetTitle("dEtajj > 3.5"); pur2->SetTitle("dEtajj > 3.5");
		eff3->SetTitle("dEtajj > 4.0"); pur3->SetTitle("dEtajj > 4.0");

		mg1->SetTitle("Efficiency; mjj_cutoff;");
		mg1->Add(eff1,"PL"); mg1->Add(eff2,"PL"); mg1->Add(eff3,"PL");
		mg2->SetTitle("Purity; mjj_cutoff;");
		mg2->Add(pur1,"PL"); mg2->Add(pur2,"PL"); mg2->Add(pur3,"PL");

		TCanvas *c = new TCanvas("c","efficiency and purity plots",800,600);
		//mg1->Draw("ALP");
		//c->BuildLegend();
		mg2->Draw("ALP");
		c->BuildLegend();
	}
}
