#ifndef HISTO_H
#define HISTO_H

#include <string>
#include <cstring>

// object containing up to 5 one-dimensional histograms
class histo{
private:
	int numhistos;

public:
	vector<TH1*> hv;

	histo(vector<string> &names, string sel, vector<double> &bins) {
		numhistos = names.size();
		for (int i = 0; i<numhistos; ++i) {
			hv.push_back(new TH1D( (names[i]+"_"+sel).c_str(),(names[i]+"_"+sel).c_str(),
							  bins[3*i],bins[3*i+1],bins[3*i+2] ));	
		}
	}
	histo(vector<string> &names, vector<string> &titles, string sel, vector<double> &bins) {
		numhistos = names.size();
		for (int i = 0; i<numhistos; ++i) {
			hv.push_back(new TH1D( (names[i]+"_"+sel).c_str(),(titles[i]).c_str(),
							  bins[3*i],bins[3*i+1],bins[3*i+2] ));	
		}
	}
	~histo() {
		for (int i=0; i<numhistos; ++i) {
			delete hv[i];
		}
	}

	int getnumhistos() {return numhistos;}

	// fill all histos with data
	void fill(vector<double> &data, double weight=1) {
		for (int i=0; i<numhistos; ++i) {
			hv[i]->Fill(data[i],weight);
		}
	}

	// write histos to a prespecified output file
	void write() {
		for (int i=0; i<numhistos; ++i) {
			hv[i]->Write();
		}
	}

	// draw a histo, default to 0th histo
	void draw(int i=0) {
		hv[i]->DrawCopy();
	}
};

#endif
