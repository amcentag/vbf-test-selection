// first time trying to make some histograms
// todo:
// 		- add another variable to plot, maybe deltaR for first 2 small jets?
//
// to include in some header file in the future:
// - histo class with modifiable number of histos
// 	 constructor allows you to choose names of histos and how many
// 	 store histos in a vector of pointers

#include <cstring>
#include <string>
#include <Math/Vector4D.h>
R__LOAD_LIBRARY(libPhysics);

using namespace std;

class histo {
public:
	TH1* HpT;
	TH1* Hsmj_delR;

	histo(string name) {
		HpT = new TH1D(("HpT_"+name).c_str(),("HpT_"+name).c_str(),20,250,1250);
		Hsmj_delR = new TH1D(("Hsmj_delR"+name).c_str(),("Hsmj_delR"+name).c_str(),28,-0.5,6.5);
	}
	~histo() {
		delete HpT;
		delete Hsmj_delR;
	}

	void fill(vector<double> &data, double weight=1) {
		HpT->Fill(data[0],weight);
		Hsmj_delR->Fill(data[1],weight);
	}
	void write() {
		HpT->Write();
		Hsmj_delR->Write();
	}
	void draw() {
		HpT->DrawCopy();
		Hsmj_delR->DrawCopy();
	}
};

void makehistos() {
	unique_ptr<TFile> F(TFile::Open("/gpfs/slac/atlas/fs1/d/zzheng/ggH/ntuple_VBF.root"));
	auto tree = F->Get<TTree>("Nominal");
	tree->SetBranchStatus("*",false);

	// set all branches to be read
	tree->SetBranchStatus("weight",true);
	double mcwt=0;
	tree->SetBranchAddress("weight",&mcwt);
	tree->SetBranchStatus("HCand_pT",true);
	double HpT=0;
	tree->SetBranchAddress("HCand_pT",&HpT);
	tree->SetBranchStatus("HCand_Phi",true);
	double HPhi=0;
	tree->SetBranchAddress("HCand_Phi",&HPhi);
	tree->SetBranchStatus("HCand_Eta",true);
	double HEta=0;
	tree->SetBranchAddress("HCand_Eta",&HEta);
	tree->SetBranchStatus("HCand_M",true);
	double HM=0;
	tree->SetBranchAddress("HCand_M",&HM);
	tree->SetBranchStatus("SmallRJets_pT",true);
	vector<double> *smjpT=0;
	tree->SetBranchAddress("SmallRJets_pT",&smjpT);
	tree->SetBranchStatus("SmallRJets_eta",true);
	vector<double> *smjEta=0;
	tree->SetBranchAddress("SmallRJets_eta",&smjEta);
	tree->SetBranchStatus("SmallRJets_phi",true);
	vector<double> *smjPhi=0;
	tree->SetBranchAddress("SmallRJets_phi",&smjPhi);
	tree->SetBranchStatus("SmallRJets_M",true);
	vector<double> *smjM=0;
	tree->SetBranchAddress("SmallRJets_M",&smjM);
	tree->SetBranchStatus("passHLT_j360_a10_lcw_sub_L1J100",true);
	int trig=0;
	tree->SetBranchAddress("passHLT_j360_a10_lcw_sub_L1J100",&trig);

	int nevents = tree->GetEntries();

	histo h1("raw");
	histo h2("trig");
	for (int i=0; i<nevents; i++) {
		tree->GetEntry(i);
		TLorentzVector pH;
		pH.SetPtEtaPhiM(HpT,HEta,HPhi,HM);
		double smjdelR = -1.0;
		if (smjpT->size() > 1) {
			TLorentzVector pj1,pj2;
			double *smjpTdat = smjpT->data();
			double *smjEtadat = smjEta->data();
			double *smjPhidat = smjPhi->data();
			double *smjMdat = smjM->data();
			pj1.SetPtEtaPhiM(smjpTdat[0],smjEtadat[0],smjPhidat[0],smjMdat[0]);
			pj2.SetPtEtaPhiM(smjpTdat[1],smjEtadat[1],smjPhidat[1],smjMdat[1]);
			smjdelR = pj1.DeltaR(pj2);
		}
		
		vector<double> rawdata {HpT,smjdelR};
		h1.fill(rawdata,1);
		if (trig) h2.fill(rawdata,1);
	}

	delete smjpT;
	delete smjEta;
	delete smjPhi;
	delete smjM;

	unique_ptr<TFile> outF(TFile::Open("outhistos.root","RECREATE"));
	outF->cd();
	h1.write();
	h2.write();
	outF->Close();
}
