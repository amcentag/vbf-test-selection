// read out pT entries in AntiKt10 branch

using namespace std;

void readpt() {
	unique_ptr<TFile> F(TFile::Open("VBF.root"));
	auto tree = F->Get<TTree>("CollectionTree");

	vector<double> *pt;
	// pt for big jets (anti-kt 1.0)
	//tree->SetBranchAddress("AntiKt10UFOCSSKSoftDropBeta100Zcut10Jets___NominalAuxDyn.pt",&pt);
	// pt for small jets (anti-kt 0.4)
	tree->SetBranchAddress("AntiKt4EMPFlowJets___NominalAuxDyn.pt",&pt);
	int nevents = tree->GetEntries();
	cout << "Num events = " << nevents << endl;
	
	for (int i=0; i<nevents; i++) {
		tree->GetEntry(i);
		int njets = pt->size();
		double *jetpt = pt->data();
		if (njets == 0) continue;
		for (int j=0; j<njets; j++) {
			cout << jetpt[j] << ',';
		}
		cout << endl;
	}
}
