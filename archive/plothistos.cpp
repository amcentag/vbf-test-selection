using namespace std;

void plothistos() {
	unique_ptr<TFile> f(TFile::Open("outhistos.root"));
	unique_ptr<TH1D> h1(f->Get<TH1D>("delEta_smalljets_raw"));
	unique_ptr<TH1D> h2(f->Get<TH1D>("delEta_smalljets_trig"));
	unique_ptr<TH1D> h3(f->Get<TH1D>("delEta_smalljets_trig_and_ptH>450"));
	if (!h1) cout << "Could not find h1";

	//h1->DrawCopy();
	TCanvas *c = new TCanvas("c");
	h1->SetLineColor(kRed);
	h2->SetLineColor(kBlue);
	h3->SetLineColor(kGreen+2);
	h1->DrawCopy("hist");
	h1->SetTitle("some title");
	h2->DrawCopy("hist same");
	h3->DrawCopy("hist same");
}
