#include <histo.h>

using namespace std;

void samplehistosvbf() {
	unique_ptr<TFile> F(TFile::Open("/gpfs/slac/atlas/fs1/d/zzheng/ggH/ntuple_VBF.root"));
	auto tree = F->Get<TTree>("Nominal");
	tree->SetBranchStatus("*",false);

	double mcwt=0;
	tree->SetBranchStatus("weight",true);
	tree->SetBranchAddress("weight",&mcwt);
	double ptH=0;
	tree->SetBranchStatus("HCand_pT",true);
	tree->SetBranchAddress("HCand_pT",&ptH);
	double etaH=0;
	tree->SetBranchStatus("HCand_Eta",true);
	tree->SetBranchAddress("HCand_Eta",&etaH);
	double phiH=0;
	tree->SetBranchStatus("HCand_Phi",true);
	tree->SetBranchAddress("HCand_Phi",&phiH);
	double mH=0;
	tree->SetBranchStatus("HCand_M",true);
	tree->SetBranchAddress("HCand_M",&mH);
	vector<double> *ptsmj=0;
	tree->SetBranchStatus("SmallRJets_pT",true);
	tree->SetBranchAddress("SmallRJets_pT",&ptsmj);
	vector<double> *etasmj=0;
	tree->SetBranchStatus("SmallRJets_eta",true);
	tree->SetBranchAddress("SmallRJets_eta",&etasmj);
	vector<double> *phismj=0;
	tree->SetBranchStatus("SmallRJets_phi",true);
	tree->SetBranchAddress("SmallRJets_phi",&phismj);
	vector<double> *msmj=0;
	tree->SetBranchStatus("SmallRJets_M",true);
	tree->SetBranchAddress("SmallRJets_M",&msmj);
	int trig=0;
	tree->SetBranchStatus("passHLT_j360_a10_lcw_sub_L1J100",true);
	tree->SetBranchAddress("passHLT_j360_a10_lcw_sub_L1J100",&trig);

	int nevents = tree->GetEntries();
	vector<string> histonames={"ptH","delEta_smalljets"};
	vector<double> histobins={50,0,1000,40,0,4};
	histo hraw(histonames,"raw",histobins);
	histo htrig(histonames,"trig",histobins);
	histo htrigcut(histonames,"trig_and_ptH>450",histobins);

	for (int i=0; i<nevents; i++) {
		tree->GetEntry(i);
		TLorentzVector pH;
		pH.SetPtEtaPhiM(ptH,etaH,phiH,mH);
		double deletasmj=-99.0;
		if (ptsmj->size() > 1) {
			TLorentzVector pj1,pj2;
			int jetcount=0;
			double *ptsmjDat = ptsmj->data();
			double *etasmjDat = etasmj->data();
			double *phismjDat = phismj->data();
			double *msmjDat = msmj->data();
			for (int j=0; j<ptsmj->size(); j++) {
				pj1.SetPtEtaPhiM(ptsmjDat[j],etasmjDat[j],phismjDat[j],msmjDat[j]);
				jetcount = j;
				if (pj1.DeltaR(pH)>1.0) {break;}
			}
			for (int j=jetcount+1; j<ptsmj->size(); j++) {
				pj2.SetPtEtaPhiM(ptsmjDat[j],etasmjDat[j],phismjDat[j],msmjDat[j]);
				jetcount = j;
				if (pj2.DeltaR(pH)>1.0) {
					deletasmj = pj1.Eta() - pj2.Eta();
					break;
				}
			}
		}
		vector<double> histodata = {ptH, abs(deletasmj)};
		hraw.fill(histodata,1);
		if (trig) { htrig.fill(histodata,1); }
		if (trig and (ptH > 450)) { htrigcut.fill(histodata,1); }
	}

	delete ptsmj;
	delete etasmj;
	delete phismj;
	delete msmj;

	unique_ptr<TFile> outF(TFile::Open("outhistos.root","RECREATE"));
	outF->cd();
	hraw.write();
	htrig.write();
	htrigcut.write();
	outF->Close();
}
